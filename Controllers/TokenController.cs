using System.Security.Claims;
using System.Text;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Authorization;

namespace net_core_jwt.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TokenController : ControllerBase
    {
        private readonly IConfiguration _config;

        private readonly ILogger<TokenController> _logger;

        public TokenController(IConfiguration config, ILogger<TokenController> logger)
        {
            _config = config;
            _logger = logger;
        }

        [HttpGet]
        [Route("/GenerateToken")]
        public IActionResult GenerateToken()
        {
            string jwtToken = "";

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_config.GetSection("JwtConfig:Key").Value);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Audience = _config.GetSection("JwtConfig:Audience").Value,
                Issuer = _config.GetSection("JwtConfig:Issuer").Value
            };
            var usernameClaim = new Claim(ClaimTypes.Name, "Nael");
            var roleClaim = new Claim(ClaimTypes.Role, "USER");

            tokenDescriptor.Subject = new ClaimsIdentity(new Claim[]{
                usernameClaim, roleClaim
            });

            tokenDescriptor.Expires = DateTime.Now.AddMinutes(10);
            tokenDescriptor.SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature);

            var token = tokenHandler.CreateToken(tokenDescriptor);
            jwtToken = tokenHandler.WriteToken(token);

            return Ok(jwtToken);
        }

        [HttpGet]
        [Route("/ValidateToken"), Authorize]
        public IActionResult ValidateToken()
        {
            return Ok("Congrats, your token is right!");
        }

    }
}